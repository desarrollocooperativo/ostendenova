<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Formula extends Master
{
    public function componente(){
    	return $this->belongsTo(Producto::class,'componente_id');
    }

    public function Producto(){
    	return $this->belongsTo(Producto::class,'producto_id');
    }
}
