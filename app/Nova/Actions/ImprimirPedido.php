<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Status;
use App\Models\Pedido;
use PDF;
use Uuid;


class ImprimirPedido extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $onlyOnDetail = true;
    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        foreach ($models as $model) {
            
            $pdf = PDF::loadView('pdf.pedido', ['pedido'=>$model,'fields'=>$fields]);
            $uuid = (string) Uuid::generate();
            $filename = storage_path('app/public/pdf/'.$uuid.'.pdf');
            $pdf->save($filename);
            return Action::download(url('storage/pdf/'.$uuid.'.pdf'),'pedido-'.$model->id.'.pdf');

        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Text::make('Transporte'),
            Date::make('Entrega')->rules('required'),
            Textarea::make('Comentarios'),
            Text::make('Caja_1'),
            Text::make('Caja_2'),
            Text::make('Caja_3'),
            Text::make('Caja_4'),
            Text::make('Caja_5'),
            Text::make('Caja_6'),
        ];
    }
}
