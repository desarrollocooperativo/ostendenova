<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Events\StockSaving as StockSavingEvent;
use App\Models\StockMovimiento;
use Auth;

class StockSavingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(StockSavingEvent $event)
    {
        StockMovimiento::create([
            "stock_id" => $event->stock->id,
            "rebabado" => $event->stock->rebabado,
            "fabrica" => $event->stock->fabrica,
            "user_id" => Auth::user()->id
        ]);
    }
}
