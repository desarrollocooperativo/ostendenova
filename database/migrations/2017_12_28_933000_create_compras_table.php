<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compras', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('cuentas_id')->unsigned();
			$table->date('fecha');
			$table->char('destino',3)->nullable();
			$table->decimal('subtotal', 18, 2)->default(0);
			$table->decimal('total', 18, 2)->default(0);
			$table->text('observaciones')->nullable();
            $table->foreign('cuentas_id')->references('id')->on('cuentas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compras');
    }
}
