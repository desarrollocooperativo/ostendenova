<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

class Produccion extends Master
{
	protected $table = "produccion";
    protected $dates = ['fecha','created_at','updated_at'];

    public function produccionlinea(){
    	return $this->hasMany(ProduccionLinea::class);
    }

    public function actualizarTotales(){
    	$lineas = $this->produccionlinea()->get();
    	$producido = count($lineas) ? $lineas->sum('producido') : 0;
    	$cantidad = count($lineas) ? $lineas->sum('cantidad') : 0;
    	$this->producido = $producido;
    	$this->cantidad = $cantidad;
    	$this->estado = ($cantidad <= $producido) ? 'T' : 'P';
    	$this->save();
    }
}
