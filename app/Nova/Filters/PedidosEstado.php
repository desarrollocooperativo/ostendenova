<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Laravel\Nova\Filters\Filter;

class PedidosEstado extends Filter
{
    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        switch ($value) {
            case 'pedido':
                return $query->whereRaw("0 = (select ifnull(sum(entregado),0) from pedido_lineas where pedido_id = pedidos.id)");
                break;

            case 'pendiente':
                return $query->whereRaw(" 0 < (select ifnull(count(*),0) from pedido_lineas where pedido_id = pedidos.id and cantidad - entregado > 0 and entregado > 0)");
                break;

            case 'entregado':
                return $query->whereRaw(" 0 = (select sum(cantidad - entregado) from pedido_lineas where pedido_id = pedidos.id)");
                break;
            
            default:
                return $query;
                break;
        }
        
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
            "Pedido"=>'pedido',
            "Pendiente"=>'pendiente',
            "Entregado"=>'entregado'
        ];
    }
}
