<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;


use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Status;
use Titasgailius\SearchRelations\SearchesRelations;


class Pedido extends Resource
{
    use SearchesRelations;
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Pedido';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';


    public static $group = 'operaciones';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];
    public static $searchRelations = [
        'cuentas' => ['razon_social' ],
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            BelongsTo::make('Cuentas')->rules('required')->searchable(),
            Date::make('fecha')->rules('required')->format('D/M/Y')->sortable(),
            Boolean::make('expreso')->rules('required'),
            Currency::make('subtotal')->exceptOnForms(),
            Number::make('Descuento','descuento')->onlyOnDetail(),
            Currency::make('total')->exceptOnForms(),
            Status::make('estado')->exceptOnForms()->loadingWhen(['pendiente'])->failedWhen(['pedido']),
            Textarea::make('observaciones'),
            HasMany::make('PedidoLinea'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            new Metrics\TotalPedidosPendientes,
            new Metrics\TotalPedidosPedidos,
            new Metrics\TotalPedidosEntregados,
            (new Metrics\TotalPedidos)->width('full'),
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new Filters\PedidosDesde,
            new Filters\PedidosHasta,
            new Filters\PedidosExpreso,
            new Filters\PedidosEstado,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new Actions\ImprimirPedido,
        ];
    }
}
