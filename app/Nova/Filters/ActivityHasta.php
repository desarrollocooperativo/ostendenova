<?php


namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\DateFilter;


class ActivityHasta extends DateFilter
{


    public function apply(Request $request, $query, $value)
    {
        return $query->where('created_at','<=', $value);
    }
    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
  	}
}