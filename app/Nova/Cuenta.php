<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;


use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Number;


use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;


class Cuenta extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Cuenta';
    //public static $with = ['tipo'];
    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'razon_social';

    public static $group = 'Agenda';

    public function title()
        {
            return (empty($this->razon_social)) ? $this->empresa.' ('.$this->tipo.')' : $this->razon_social.' ('.$this->tipo.')';
        }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'razon_social','localidad'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Select::make('Tipo','tipo')->options([
                'C'=>'Cliente',
                'P'=>'Proveedor',
            ])->displayUsingLabels()->rules('required','in:C,P','max:1'),
            
            Text::make('Razón Social','razon_social')
                ->sortable()
                ->rules('required', 'max:255'),
            
            Text::make('CUIT','cuit')->rules('required','string','max:11'),

            Number::make('Descuento','descuento')->rules('required')->hideFromIndex(),
            
            Text::make('Domicilio','domicilio')->rules('required','max:255')->hideFromIndex(),
            
            Text::make('Localidad','localidad')->rules('required','max:255')->hideFromIndex(),

            BelongsTo::make('Provincias')->hideFromIndex(),

            Text::make('Teléfono','telefono')->rules('required','max:255'),

            Text::make('Email','email')->rules('required','email'),

            HasMany::make('Contactos'),
            HasMany::make('Pedidos'),


        ];
    }
    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            new Metrics\ClientesPorMes,
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new Filters\CuentaTipo,
            new Filters\CuentaProvincia,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new DownloadExcel,
        ];
    }
}
