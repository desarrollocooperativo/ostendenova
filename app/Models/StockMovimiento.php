<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockMovimiento extends Model
{
    protected $guarded = [];
    protected $appends = ['titulo'];

    public function getTituloAttribute(){
    	return $this->Stock->titulo;
    }

    public function Stock(){
    	return $this->belongsTo(Stock::class);
    }

    public function User(){
    	return $this->belongsTo(User::class);
    }
}
