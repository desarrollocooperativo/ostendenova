<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function grupos(){
        return $this->belongsTo(Grupo::class);
    }

    public function permiso($modelo){
        $permiso = Permiso::where('grupos_id',$this->grupos_id)->where('modelo','=',$modelo)->first();
        return (empty($permiso)) ? 'D':$permiso->accion;
    }
}
