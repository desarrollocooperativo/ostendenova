<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Events\StockSaving;
use DB;

class Stock extends Master
{
	protected $table = 'stock';
	protected $appends = ['comprometido','posible','valor','titulo','moneda'];
    protected $fillable = ['producto_id','rebabado','fabrica'];

    public function stockmovimiento(){
    	return $this->hasMany(StockMovimiento::class,'stock_id');
    }

    public function producto(){
    	return $this->belongsTo(Producto::class,'producto_id');
    }

    public function getComprometidoAttribute(){
    	/* TODO
    	sumar cantidad - entregado en lineas de pedido para calcular comprometido
    	*/
    	return 0;
    }
    public function getPosibleAttribute(){
        //producto terminado

        if(!empty($this->producto) && $this->Producto->tipo == 1){
            $query = "select min(floor((s.fabrica + s.rebabado)/f.cantidad)) as posib 
                from formulas f
                inner join stock s on s.producto_id = f.componente_id
                where f.producto_id = ?
                group by  f.producto_id
            ";
            $posible = DB::select($query,[$this->producto_id]);
            return count($posible) ? $posible[0]->posib : 0;
        } else {
            return $this->reb + $this->fab;
        }
    }
    public function getValorAttribute(){
        return !empty($this->producto) ? ($this->fabrica + $this->rebabado) * $this->Producto->precio : 0;
    }

    public function getMonedaAttribute(){
        return !empty($this->producto) ? $this->producto->moneda : null;
    }

    public function getTituloAttribute(){
    	return $this->producto->descripcion;
    }


    protected $dispatchesEvents = [
        'saved' => StockSaving::class,
    ];
}