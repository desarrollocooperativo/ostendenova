<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo');
            $table->string('descripcion');
			$table->string('codigo');
            $table->string('cod_mat')->nullable();
            $table->string('medidas')->nullable();
            $table->char('origen',1);
            $table->integer('bocas')->nullable();
            $table->integer('stock_min')->nullable();
			$table->integer('stock_max')->nullable();
            $table->string('codigo_proveedor')->nullable();
            $table->char('moneda',3)->default('ARS');
            $table->decimal('precio',14,2)->nullable();
            $table->decimal('precio_proveedor',14,2)->nullable();
            $table->integer('unidad_id')->unsigned()->nullable();
            $table->integer('cuenta_id')->unsigned()->nullable();
			$table->integer('categoria_id')->unsigned()->nullable();
            $table->foreign('unidad_id')->references('id')->on('unidades');
            $table->foreign('categoria_id')->references('id')->on('categorias');
            $table->foreign('cuenta_id')->references('id')->on('cuentas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
