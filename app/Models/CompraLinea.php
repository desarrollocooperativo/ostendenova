<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Events\CompraLineaSaving;
use App\Events\CompraLineaSaved;
use App\Events\CompraLineaUpdated;
use App\Events\CompraLineaCreated;
use DB;
use Carbon\Carbon;


class CompraLinea extends Master
{
    public function Compra(){
        return $this->belongsTo(Compra::class);
    }

    public function producto(){
    	return $this->belongsTo(Producto::class);
    }

    public function entregar(){
    	$this->entregado = $this->cantidad;
    	$this->save();
    }

    protected $dispatchesEvents = [
        'saving' => CompraLineaSaving::class,
        'saved' => CompraLineaSaved::class,
        'updating' => CompraLineaUpdated::class,
        'created' => CompraLineaCreated::class,
    ];

}
