<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulas', function (Blueprint $table) {
            $table->increments('id');
			$table->decimal('cantidad', 8, 2);
			$table->integer('producto_id')->unsigned();
			$table->integer('componente_id')->unsigned();
			$table->foreign('producto_id')->references('id')->on('productos')->unsigned();
			$table->foreign('componente_id')->references('id')->on('productos')->unsigned();
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulas');
    }
}
