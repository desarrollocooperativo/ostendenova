<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    public function cuentas(){
    	return $this->hasMany(Cuenta::class);
    }
}
