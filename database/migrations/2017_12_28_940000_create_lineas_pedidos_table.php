<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLineasPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_lineas', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('pedido_id')->unsigned();
			$table->integer('producto_id')->unsigned();
			$table->decimal('cantidad', 8, 2)->default(0);
            $table->decimal('entregado',8,2)->default(0);
			$table->decimal('precio', 8, 2)->default(0);
			$table->decimal('importe', 8, 2)->default(0);
            $table->decimal('iva', 8, 2)->default(21);
			$table->foreign('pedido_id')->references('id')->on('pedidos')->unsigned();
			$table->foreign('producto_id')->references('id')->on('productos')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lineas_pedidos');
    }
}
