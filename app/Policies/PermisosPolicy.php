<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Permiso;

class PermisosPolicy extends GenericPolicy
{

    public function viewAny(User $user)
    {
        $p = $user->permiso(Permiso::class);
        return  $p == 'R' || $p == 'W';
    }

    /**
     * Determine whether the user can create cuentas.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $p = $user->permiso(Permiso::class);
        return  $p == 'W';
    }

}
