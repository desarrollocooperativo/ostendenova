<?php

namespace App\Listeners;

use App\Events\ProduccionLineaUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\ProduccionLineaUpdated as ProduccionLineaUpdatedEvent;
use App\Models\ProduccionLinea;
use App\Models\Stock;
class ProduccionLineaUpdatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CompraLineaUpdated  $event
     * @return void
     */
    public function handle(ProduccionLineaUpdatedEvent $event)
    {
        $original = (object) $event->produccion_linea->getOriginal();
        
        if($event->produccion_linea->producido != $original->producido){
            $dif = $event->produccion_linea->producido - $original->producido;
            $producto = $event->produccion_linea->Producto;
            if(empty($producto->Stock)){
                Stock::create(['producto_id'=>$producto->id,'rebabado'=> $dif,'fabrica'=>0]);
            } else {
                $s = Stock::where('producto_id','=',$producto->id)->first();
                $s->rebabado += $dif;
                $s->save();
            }


            if($producto->formulas()->count()){
                $dif = $dif * -1; //invierto el signo
                    
                foreach($producto->formulas as $formula){
                    $cant = $dif * $formula->cantidad;
                    if(empty($formula->componente->Stock)){
                        Stock::create(['producto_id'=>$formula->componente->id,'fabrica'=> $cant,'rebabado'=>0]);
                    } else {
                        $s = Stock::where('producto_id','=',$formula->componente->id)->first();
                        if($cant < 1){
                            $s->fabrica += $cant;
                        }else{
                            $s->fabrica -= $cant;
                        }
                        $s->save();
                    }

                }
            }
        }
    }
}
