<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\CompraLinea;

class CompraLineaCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $compra_linea;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(CompraLinea $compra_linea)
    {
        $this->compra_linea = $compra_linea;
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
