<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Laravel\Nova\Filters\Filter;

class ComprasEstado extends Filter
{
    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        switch ($value) {
            case 'pedido':
                return $query->whereRaw("0 = (select ifnull(sum(entregado),0) from compra_lineas where compra_id = compras.id)");
                break;

            case 'incompleto':
                return $query->whereRaw(" 0 < (select ifnull(count(*),0) from compra_lineas where compra_id = compras.id and cantidad - entregado > 0 and entregado > 0)");
                break;

            case 'entregado':
                return $query->whereRaw(" 0 = (select sum(cantidad - entregado) from compra_lineas where compra_id = compras.id)");
                break;
            
            default:
                return $query;
                break;
        }
        
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
            "Pedido"=>'pedido',
            "Incompleto"=>'incompleto',
            "Entregado"=>'entregado'
        ];
    }
}
