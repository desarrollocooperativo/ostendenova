<?php

namespace App\Listeners;

use App\Events\CompraLineaSaving;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\CompraLineaSaving as CompraLineaSavingEvent;
use App\Models\CompraLinea;
use App\Models\Cotizacion;

class CompraLineaListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CompraLineaSaving  $event
     * @return void
     */
    public function handle(CompraLineaSavingEvent $event)
    {
        if(empty($event->compra_linea->precio)){
            $event->compra_linea->precio = empty($event->compra_linea->producto->precio) ? 0 : $event->compra_linea->producto->precio;
            $moneda = empty($event->compra_linea->producto->moneda) ? 0 : $event->compra_linea->producto->moneda;
            //dolar
            if($moneda == 1){
                $cotizacion = Cotizacion::orderBy('created_at','desc')->first();
                $event->compra_linea->precio = $event->compra_linea->precio * $cotizacion->cotizacion;
            }

        }
        $event->compra_linea->subtotal = $event->compra_linea->cantidad * $event->compra_linea->precio;
        $event->compra_linea->total_iva = ($event->compra_linea->subtotal * $event->compra_linea->iva) / 100;
        $event->compra_linea->total = $event->compra_linea->subtotal + $event->compra_linea->total_iva;
        
    }
}
