<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('producto_id')->unsigned()->unique();
			$table->decimal('rebabado', 8, 2)->default(0);
			$table->decimal('fabrica', 8, 2)->default(0);
            $table->foreign('producto_id')->references('id')->on('productos')->unsigned();
            $table->timestamps();
        });

        Schema::create('stock_movimientos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stock_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->decimal('rebabado', 8, 2)->default(0);
            $table->decimal('fabrica', 8, 2)->default(0);
            $table->foreign('stock_id')->references('id')->on('stock')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_movimientos');
        Schema::dropIfExists('stock');
    }
}
