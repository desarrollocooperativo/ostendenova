<?php

namespace App\Listeners;

use App\Events\CompraLineaSaved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\CompraLineaSaved as CompraLineaSavedEvent;
use App\Models\CompraLinea;

class CompraLineaSavedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CompraLineaSaved  $event
     * @return void
     */
    public function handle(CompraLineaSavedEvent $event)
    {

        $event->compra_linea->compra->actualizarTotales();


    }
}
