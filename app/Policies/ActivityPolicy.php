<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ActivityPolicy
{
    use HandlesAuthorization;

    

    /**
     * Determine whether the user can view the cuenta.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Cuenta  $cuenta
     * @return mixed
     */
    public function view(User $user, $model)
    {
        return  $user->grupos_id == 1;
    }

    /**
     * Determine whether the user can create cuentas.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the cuenta.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Cuenta  $cuenta
     * @return mixed
     */
    public function update(User $user, $model)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the cuenta.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Cuenta  $cuenta
     * @return mixed
     */
    public function delete(User $user, $model)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the cuenta.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Cuenta  $cuenta
     * @return mixed
     */
    public function restore(User $user, $model)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the cuenta.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Cuenta  $cuenta
     * @return mixed
     */
    public function forceDelete(User $user, $model)
    {
        return false;
    }
}
