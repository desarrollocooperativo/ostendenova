<html>
	<head>
		<style>
			td, th {
				border: solid 1px black;
			}
			.sinborde {
				border: none !important;
			}
			.separador {
				height: 20px;
			}
			.etiqueta {
				font-weight: bold;
			}
			th {
				background-color: #dedede;
			}
		</style>
	</head>
	<body>
		<table align="center" width="100%" cellspacing="0">
			<tr>
				<td width="50%" class="sinborde">
					<img src="/logo-print.png">
					<div>
						<b>Pronevia s.a.</b><br>
						{{env('DOMICILIO')}}<BR>
						Tel: {{env('TELEFONO')}} / Fax: {{env('FAX')}}<br>
						CUIT: {{env('CUIT')}}
					</div>
				</td>
				<td width="50%">
					<table width="100%" cellspacing="0">
						<tr>
							<td align="center" class="sinborde">
								<h3>Órden de compra</h3>
								<div>Estado: <b>{{$fields->estado == 'A' ? 'Abierto':'Cerrada'}}</b></div>
								<h4>Número</h4>
								<div>{{$compra->id}}</div>
								<h4>Fecha</h4>
								<div>{{$compra->fecha->format('d/m/Y')}}</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td class="separador sinborde">&nbsp;</td></tr>
			<tr>
				<td>Proveedor: {{$compra->Cuentas->activity_ref}}</td>
				<td>Código: {{$compra->Cuentas->codigo}}</td>
			</tr>
			<tr>
				@if($compra->Cuentas->Contactos()->count() > 0)
				<td>Contacto: {{$compra->Cuentas->Contactos()->first()->nombre}}</td>
				<td>Email: {{$compra->Cuentas->Contactos()->first()->email}}</td>
				@else
				<td>Contacto: </td>
				<td>Email: {{$compra->Cuentas->email}}</td>
				@endif
			</tr>
			<tr>
				<td>Dirección: {{$compra->Cuentas->direccion}}</td>
				<td>Teléfono: {{$compra->Cuentas->telefono}}</td>
			</tr>
			<tr>
				<td>CUIT: {{$compra->Cuentas->cuit}}</td>
				<td>Fax: {{$compra->Cuentas->fax}}</td>
			</tr>
			<tr><td class="separador sinborde">&nbsp;</td></tr>
			<tr>
				<td class="sinborde" colspan="3">
					<table width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>It.</th>
								<th>Código</th>
								<th>Producto</th>
								<th>Cant.</th>
								<th>Uni.</th>
								<th>IVA</th>
								<th>Precio</th>
								<th>Importe</th>
							</tr>
						</thead>
						<tbody>
							@foreach($compra->compralinea as $linea)
							<tr>
								<td>{{$loop->iteration}}</td>
								<td>{{$linea->producto->codigo}}</td>
								<td>{{$linea->producto->descripcion}}</td>
								<td align="right">{{$linea->cantidad}}</td>
								<td>{{$linea->producto->unidad->unidad}}</td>
								<td align="right">{{$linea->iva}}%</td>
								<td align="right">{{number_format($linea->precio,2,',','.')}}</td>
								<td align="right">{{number_format($linea->subtotal,2,',','.')}}</td>
							</tr>
							@endforeach
							<tr>
								<td colspan="5">&nbsp;</td>
								<td colspan="2">Subtotal</td>
								<td align="right">{{number_format($compra->compralinea->sum("subtotal"),2,',','.')}}</td>
							</tr>							
							<tr>
								<td colspan="5">&nbsp;</td>
								<td colspan="2">IVA</td>
								<td align="right">{{number_format($compra->compralinea->sum("total_iva"),2,',','.')}}</td>
							</tr>
							<tr>
								<td colspan="5">&nbsp;</td>
								<td colspan="2">Total</td>
								<td align="right">{{number_format($compra->compralinea->sum("total"),2,',','.')}}</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr><td class="separador sinborde">&nbsp;</td></tr>
			<tr>
				<td colspan="8">
					<div>
						<b>Condiciones Generales:</b>
						 <br>EN CASO DE NO RECIBIR ORDEN DE COMPRA MODIFICATORIA DE ESTA, LOS PEDIDOS POTENCIALES SE TRANSFORMARÁN EN FIRMES. 
						 <br>LOS MATERIALES REMITIDOS Y FACTURADOS DEBERAN INDICAR CLARAMENTE MEDIDA Y CÓDIGO. 
						 <br>PRESENTAR CERTIFICADOS DE FABRICACIÓN CORRESPONDIENTES.
						 <br>LOS MATERIALES SE ENTREGARAN EN EMBALAJES ADECUADOS, RESISTENTES Y MANIOBRABLES, IDENTIFICADO N° DE REMITO AL QUE CORRESPONDE, CANTIDAD, FECHA DE ENTREGA Y N° DE LOTE Y/0 CERTIFICADO SI CORRESPONDIERE. 
						 <br>CONFIRMAR LA RECEPCIÓN DE LA PRESENTE EN UN PLAZO MÁXIMO DE 2 DÍAS, CASO CONTRARIO LA DAREMOS POR ACEPTADA EN TODOS SUS TERMI NOS Y CONDICIONES.
					</div>
				</td>
			</tr>
			<tr><td class="separador sinborde">&nbsp;</td></tr>
			<tr>
				<td colspan="3" class="sinborde">
					<table width="100%" cellspacing="0">
						<tr>
							<td width="70%">
								<div><b>Condiciones de pago:</b>{{$compra->condicion_pago}}</div>
							</td>
							<td width="30%" valign="top" align="center" rowspan="2" height="160">
								<div><b>Confeccionó:</b></div>
								<div>&nbsp;</div>
								<div>&nbsp;</div>
								<div>&nbsp;</div>
								<div>&nbsp;</div>
								<div>&nbsp;</div>
							</td>
						</tr>
						<tr>
							<td>
								<div><b>Lugar de entrega: </b>{{$fields->entrega}}</div>
							</td>
						</tr>
						<tr>
							<td width="70%">
								<div><b>Fecha de entrega: </b>{{$fields->fecha}}</div>
							</td>
							<td width="30%" valign="top" align="center" rowspan="2" >
								<div>
								<b>Autorizo:</b>
								</div>
								<div>&nbsp;</div>
								<div>&nbsp;</div>
								<div>&nbsp;</div>
								<div>&nbsp;</div>
								<div>&nbsp;</div>
							</td>
						</tr>
						<tr>
							<td>
								<div><b>Horario de recepción: </b>{{$fields->horario}}</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>