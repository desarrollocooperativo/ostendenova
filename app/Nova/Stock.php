<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;


use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Currency;

class Stock extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Stock';

    public static $group = 'Stock';

    

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'producto_id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id','producto_id'
    ];

    public static $globallySearchable = false;
    
    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            BelongsTo::make('Producto'),
            
            Text::make('Fábrica','fabrica')->rules('required','numeric'),
            
            Text::make('Rebabado','rebabado')->rules('required','numeric'),

            Text::make('Comprometido','comprometido')->exceptOnForms(),
            Number::make('Posible','posible')->exceptOnForms(),
            Select::make('Moneda','moneda')->options([
                '0'=>'ARS',
                '1'=>'USD',
            ])->displayUsingLabels()->exceptOnForms(),
            Currency::make('Valor','valor')->exceptOnForms(),

            HasMany::make('StockMovimiento'),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

}
