<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GruposTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ProvinciasTableSeeder::class);
        $this->call(PermisosTableSeeder::class);
        $this->call(UnidadesTableSeeder::class);
        $this->call(CategoriasTableSeeder::class);
    }
}
class UsersTableSeeder extends Seeder
{
	public function run()
	{
		DB::table('users')->insert([
			"name" => "Admin",
			"email" => "info@ostende.com.ar",
			"password" => bcrypt('123456'),
            "grupos_id" => 1,
		]);
	}
}
class GruposTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('grupos')->insert([
            "grupo" => "Administradores",
        ]);
        DB::table('grupos')->insert([
            "grupo" => "Usuarios",
        ]);
    }
}

class PermisosTableSeeder extends Seeder
{
    public function run(){
        $modelos = config('app.permisos_modelos');
        foreach($modelos as $modelo => $nombre){
            DB::table('permisos')->insert([
                'grupos_id' => 1,
                'modelo' => $modelo,
                'accion' => ($nombre == 'StockMovimiento') ? 'R':'W',
            ]);
            DB::table('permisos')->insert([
                'grupos_id' => 2,
                'modelo' => $modelo,
                'accion' => 'R',
            ]);
        }
    }
}

class ProvinciasTableSeeder extends Seeder
{
	public function run()
	{
		DB::statement("INSERT INTO `provincias` (`provincia`,x_provincia) VALUES
                    ('Buenos Aires','BUENOS AIRES BS AS'),
                    ('Capital Federal','CAPITAL FEDERAL CABA'),
                    ('Catamarca','CATAMARCA'),
                    ('Chaco','CHACO'),
                    ('Chubut','CHUBUT'),
                    ('Córdoba','CORDOBA CÓRDOBA'),
                    ('Corrientes', 'CORRIENTES'),
                    ('Entre Ríos','ENTRE RIOS ENTRE RÍOS'),
                    ('Formosa','FORMOSA'),
                    ('Jujuy', 'JUJUY'),
                    ('La Pampa', 'LA PAMPA'),
                    ('La Rioja', 'LA RIOJA'),
                    ('Mendoza', 'MENDOZA'),
                    ('Misiones', 'MISIONES'),
                    ('Neuquén', 'NEUQUEN NEUQUÉN'),
                    ('Río Negro','RIO NEGRO RÍO NEGRO'),
                    ('Salta', 'SALTA'),
                    ('San Juan', 'SAN JUAN'),
                    ('San Luis', 'SAN LUIS'),
                    ('Santa Cruz', 'SANTA CRUZ'),
                    ('Santa Fe', 'SANTA FE SANTA FÉ'),
                    ('Santiago del Estero', 'SANTIAGO DEL ESTERO'),
                    ('Tierra del Fuego', 'TIERRA DEL FUEGO'),
                    ('Tucumán', 'TUCUMAN TUCUMÁN');
                ");
	}
}
 
class UnidadesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('unidades')->insert([
            "unidad" => "Masa (Kg)",
        ]);
        DB::table('unidades')->insert([
            "unidad" => "Unidad",
        ]);
        DB::table('unidades')->insert([
            "unidad" => "Metros (m)",
        ]);
        DB::table('unidades')->insert([
            "unidad" => "Litros (L)",
        ]);
    }
}
class CategoriasTableSeeder extends Seeder
{
    public function run()
    {
        DB::statement("
            insert into categorias (id, categoria) VALUES
                (3, 'Otra categoría'),
                (9, 'Bujes Para Terminar'),
                (11, 'BUJES ARMADOS'),
                (12, 'CAUCHO FORMULA'),
                (13, 'QUIMICA'),
                (14, 'Bujes Semiblock'),
                (17, 'Tubo mecanizado.'),
                (19, 'Caños'),
                (20, 'Formula OSTENDE'),
                (21, 'MEZCLA BASE'),
                (23, 'CAUCHO'),
                (24, 'CARGA'),
                (25, 'ABRASIVO'),
                (26, 'PLASTIFICANTE'),
                (27, 'FORMULA OSTENDE'),
                (28, 'FORMULA OSTENDE'),
                (29, 'TUBO TREFILADO'),
                (30, 'Caño Cortado'),
                (33, 'Buje'),
                (34, 'Caño Cortado y ESTAMPADO'),
                (35, 'Bujes de goma'),
                (36, 'Bujes tercerizados'),
                (37, 'Bieletas'),
                (38, 'Guardafango'),
                (39, 'chapa'),
                (40, 'Materia Prima'),
                (41, 'BUJE TENSOR'),
                (42, 'MEZCLA  BASE'),
                (43, 'O ring'),
                (44, 'terc'),
                (45, 'Meta'),
                (46, 'TUBO TRAFILADO'),
                (47, 'Bujes Silent Block'),
                (48, 'ACERO'),
                (49, 'Packaging'),
                (50, 'Fundicion Gris'),
                (51, 'Arandela'),
                (53, 'Varios'),
                (54, 'Acero Mecanizado.'),
                (55, 'Herramientas'),
                (56, 'Perno Tensor'),
                (57, 'terce'),
                (58, 'Antivibratorio'),
                (59, 'FRESA'),
                (60, 'Inserto'),
                (61, 'INSERTO'),
                (62, 'INSERTO'),
                (63, 'INSERTO'),
                (64, 'Industria'),
                (65, 'Bulon'),
                (66, 'INDUMENTARIA'),
                (67, 'Taco'),
                (68, 'NUCLEO'),
                (69, 'Protección'),
                (70, 'vastago'),
                (71, 'SELLO'),
                (72, 'PLACA'),
                (73, 'Arandela.'),
                (74, 'PVC Recuperado'),
                (75, 'inyección de Plásticos'),
                (76, 'Tope Gatwick'),
                (77, 'PVC'),
                (78, 'TUBO'),
                (79, 'Poliamida 6'),
                (80, 'PIEZAS ESPECIALES'),
                (81, 'fleje de chapa'),
                (82, 'fleje de chapa'),
                (83, 'Tornillo'),
                (84, 'PLACA  ALUMINIO'),
                (85, 'Polipropileno N1100'),
                (86, 'OTROS'),
                (87, 'PP+Fibra'),
                (88, 'pintura'),
                (89, 'MOLIDO'),
                (90, 'VULCANIZADO'),
                (91, 'COMPUESTO'),
                (92, 'GRANALLA');
            ");
    }
} 