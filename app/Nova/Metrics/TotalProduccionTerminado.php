<?php

namespace App\Nova\Metrics;

use Illuminate\Http\Request;
use Laravel\Nova\Metrics\Value;
use App\Models\Produccion;
use App\Models\ProduccionLinea;
use DB;

class TotalProduccionTerminado extends Value
{
    /**
     * Calculate the value of the metric.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function calculate(Request $request)
    {
        return $this->sum($request, Produccion::whereRaw('producido >= cantidad')->whereRaw('producido > 0'), 'cantidad','created_at')->suffix(' unidades')->format('0');
    }

    /**
     * Get the ranges available for the metric.
     *
     * @return array
     */
    public function ranges()
    {
        return [
            1 => '24 hs',
            7 => 'Ult. 7 días',
            30 => '30 Días',
            60 => '60 Días',
            365 => '365 Días',
        ];
    }

    /**
     * Determine for how many minutes the metric should be cached.
     *
     * @return  \DateTimeInterface|\DateInterval|float|int
     */
    public function cacheFor()
    {
        // return now()->addMinutes(5);
    }

    /**
     * Get the URI key for the metric.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'total-produccion-terminado';
    }
}
