<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Cuenta extends Master
{
    protected $appends = ['direccion'];

    public function Provincias(){
    	return $this->belongsTo(Provincia::class);
    }

    public function Contactos(){
    	return $this->hasMany(Contacto::class,'cuentas_id');
    }
    
    protected function getActivityRefAttribute(){
		return $this->razon_social;
	}

	public function pedidos(){
		return $this->hasMany(Pedido::class,'cuentas_id');
	}

    public function getDireccionAttribute(){
        return $this->domicilio.", ".$this->localidad.", ".$this->codpos.", ".$this->Provincias->provincia;
    }
}
