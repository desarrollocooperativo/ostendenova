<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Master extends Model
{
    use LogsActivity;

	protected static $logAttributes = ['*'];

	protected $appends = ['activity_ref'];

	protected function getActivityRefAttribute(){
		return true;
	}
}
