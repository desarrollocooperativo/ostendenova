<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

class Producto extends Master
{
    public function unidad(){
    	return $this->belongsTo(Unidad::class,'unidad_id');
    }

    public function categorias(){
    	return $this->belongsTo(Categoria::class,'categoria_id');
    }

    public function proveedor(){
    	return $this->belongsTo(Cuenta::class,'cuenta_id');
    }

    public function formulas(){
    	return $this->hasMany(Formula::class);
    }

    public static function promedio_diario($producto_id){
    	//promedio diario de ventas de los últimos X meses
    	$meses = config('app.meses_promedio');
    	$hoy = Carbon::now();
    	$fecha = $hoy->copy()->subMonths($meses)->format('Y-m-d');
    	$dias = $hoy->diffInDays($hoy->copy()->subMonths($meses));
    	$producto_id = (empty($producto_id)) ? 0 : $producto_id;

    	$promedio = DB::select("select ifnull(sum(l.cantidad) / ? ,0) as promedio
				from pedido_lineas l
				inner join pedidos p on l.pedido_id = p.id
				where p.fecha >= ? 
				and l.producto_id = ?
				",[$dias,$fecha,$producto_id]);
    	return $promedio[0]->promedio;
    }

    public function Stock(){
        return $this->hasOne(Stock::class);
    }
}
