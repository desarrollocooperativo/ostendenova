<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Master
{
    public function grupos(){
    	return $this->belongsTo(Grupo::class);
    }


    protected function getActivityRefAttribute(){
    	$acciones = [
    		"D" => 'Denegado',
    		"R" => 'Lectura',
    		"W" => 'Escritura',
    	];
		return str_replace('App\\Models\\','',$this->modelo).' ('.$acciones[$this->accion].')';
	}
}
