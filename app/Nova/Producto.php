<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Currency;
class Producto extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Producto';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'descripcion';

    public static $group = 'Catálogo';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id','descripcion','codigo'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $promedio = $this->model()->promedio_diario($this->id);
            
        $fields =  [
            ID::make()->sortable(),
            Select::make('Tipo','tipo')->options([
                '0'=>'Materia Prima',
                '1'=>'Pre-Terminado',
                '2'=>'Terminado',
                '3'=>'Tercerizado',
            ])->displayUsingLabels()->rules('required','in:0,1,2,3','max:1'),
            Text::make('Código','codigo')->sortable()->rules('required','max:255'),
            Text::make('Descripción','descripcion')->sortable()->rules('required','max:255'),
            Text::make('Código Matriz','cod_mat')->sortable()->rules('max:255')->hideFromIndex(),
            Select::make('Origen','origen')->options([
                'I'=>'Interno',
                'E'=>'Externo',
            ])->displayUsingLabels()->rules('required','in:I,E','max:1')->hideFromIndex(),
            BelongsTo::make('Unidad')->hideFromIndex(),
            Number::make('Bocas','bocas')->rules('numeric','nullable')->hideFromIndex(),
            BelongsTo::make('Categorias')->rules('nullable')->hideFromIndex(),
            BelongsTo::make('Cuentas','proveedor')->rules('nullable')->hideFromIndex(),
            Text::make('Código Prov.','codigo_proveedor')->sortable()->rules('nullable','max:255')->hideFromIndex(),
            Select::make('Moneda','moneda')->options([
                '0'=>'ARS',
                '1'=>'USD',
            ])->displayUsingLabels()->rules('required','in:0,1','nullable')->hideFromIndex(),
            Currency::make('Precio','precio')->sortable()->rules('nullable','numeric')->format('%.2n')->hideFromIndex(),
            Currency::make('Precio Prov.','precio_proveedor')->sortable()->rules('nullable','numeric')->format('%.2n')->hideFromIndex(),
            Number::make('Min. Días','stock_min')->resolveUsing(function ($dias) use ($promedio) {
                    $cant = $dias * $promedio; 
                    return  "$cant ($dias días)";
                })->hideFromIndex(),
            Number::make('Max. Días','stock_max')->resolveUsing(function ($dias) use ($promedio) {
                    $cant = $dias * $promedio; 
                    return  "$cant ($dias días)";
                })->hideFromIndex(),
            HasOne::make('Stock','stock')->hideFromIndex(),
        ];

        $fields[] = HasMany::make('Formulas','formulas')->hideFromIndex();
        
        return $fields;
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
