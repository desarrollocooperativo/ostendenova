<?php

namespace App\Listeners;

use App\Events\CompraLineaUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\CompraLineaUpdated as CompraLineaUpdatedEvent;
use App\Models\CompraLinea;
use App\Models\Stock;
class CompraLineaUpdatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CompraLineaUpdated  $event
     * @return void
     */
    public function handle(CompraLineaUpdatedEvent $event)
    {
        $original = (object) $event->compra_linea->getOriginal();
        
        if($event->compra_linea->entregado != $original->entregado){
            $dif = $event->compra_linea->entregado - $original->entregado;
            if(empty($event->compra_linea->Producto->Stock)){
                Stock::create(['producto_id'=>$event->compra_linea->Producto->id,'rebabado'=> 0,'fabrica'=>$dif]);
            } else {
                $s = Stock::where('producto_id','=',$event->compra_linea->Producto->id)->first();
                $s->fabrica += $dif;
                $s->save();
            }
        }
    }
}
