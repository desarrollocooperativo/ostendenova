<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Grupo;

class GruposPolicy extends GenericPolicy
{

    public function viewAny(User $user)
    {
        $p = $user->permiso(Grupo::class);
        return  $p == 'R' || $p == 'W';
    }

    /**
     * Determine whether the user can create cuentas.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $p = $user->permiso(Grupo::class);
        return  $p == 'W';
    }

}
