<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLineasCompras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
		 Schema::create('compra_lineas', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('compra_id')->unsigned();
			$table->integer('producto_id')->unsigned();
			$table->decimal('cantidad', 18, 2);
            $table->decimal('entregado', 18, 2)->default(0);
            $table->decimal('iva',18,2);
			$table->decimal('precio',18,2)->nullable()->default(0);
            $table->decimal('subtotal',18,2)->default(0);
            $table->decimal('total_iva',18,2)->default(0);
            $table->decimal('total',18,2)->default(0);
            $table->foreign('compra_id')->references('id')->on('compras')->unsigned();
			$table->foreign('producto_id')->references('id')->on('productos')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compra_lineas');
    }
}
