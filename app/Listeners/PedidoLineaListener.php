<?php

namespace App\Listeners;

use App\Events\PedidoLineasSaving;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\PedidoLineaSaving as PedidoLineaSavingEvent;
use App\Models\PedidoLinea;


class PedidoLineaListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PedidoLineasSaving  $event
     * @return void
     */
    public function handle(PedidoLineaSavingEvent $event)
    {
        $event->pedido_linea->pedido->actualizarTotales();
    }
}
