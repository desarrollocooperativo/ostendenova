<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Producto;

class ProductosPolicy extends GenericPolicy
{

    public function viewAny(User $user)
    {
        $p = $user->permiso(Producto::class);
        return  $p == 'R' || $p == 'W';
    }

    /**
     * Determine whether the user can create cuentas.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $p = $user->permiso(User::class);
        return  $p == 'W';
    }

}
