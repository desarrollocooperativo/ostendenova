<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Events\PedidoLineaSaving;
use DB;
use Carbon\Carbon;


class PedidoLinea extends Master
{

	protected $appends = ['precio','importe'];

    public function pedido(){
    	return $this->belongsTo(Pedido::class);
    }
    public function producto(){
    	return $this->belongsTo(Producto::class);
    }

    public function getPrecioAttribute(){
    	return (!empty($this->producto)) ? $this->producto->precio : 0;
    }

    public function getImporteAttribute(){
    	return (!empty($this->producto)) ? $this->producto->precio * $this->cantidad : 0;
    }

    public function entregar(){
    	$this->entregado = $this->cantidad;
    	$this->save();
    }

    protected $dispatchesEvents = [
        'saved' => PedidoLineaSaving::class,
    ];
}
