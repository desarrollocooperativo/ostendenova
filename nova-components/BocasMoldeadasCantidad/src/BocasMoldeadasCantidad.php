<?php

namespace Cardumen\BocasMoldeadasCantidad;

use Laravel\Nova\Fields\Field;

use Laravel\Nova\Http\Requests\NovaRequest;

class BocasMoldeadasCantidad extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'bocas-moldeadas-cantidad';

    public function producto($producto){
    	return $this->withMeta(['producto' => $producto]);
    }

    public function bocas($bocas,$producto_id){
    	if(empty($bocas) && !empty($producto_id)){
    		$p = \App\Models\ProduccionLinea::find($producto_id);
    		$bocas = $p->bocas;
    	}
    	return $this->withMeta(['bocas' => $bocas]);
    }
    
    public function moldeadas($moldeadas){
    	return $this->withMeta(['moldeadas' => $moldeadas]);
    }

    public function cantidad($cantidad){
    	return $this->withMeta(['cantidad' => $cantidad]);
    }
	
	protected function fillAttributeFromRequest(NovaRequest $request, $requestAttribute, $model, $attribute)
    {
        if ($request->exists('bocas')) {
            $model->bocas = $request['bocas'];
        }
        if ($request->exists('moldeadas')) {
            $model->moldeadas = $request['moldeadas'];
        }
        if ($request->exists('cantidad')) {
            $model->cantidad = $request['cantidad'];
        }
        if ($request->exists('producto')) {
            $model->producto_id = $request['producto'];
        }
    }

    
}
