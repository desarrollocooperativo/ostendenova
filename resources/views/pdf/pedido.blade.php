<html>
	<head>
		<style>
			td, th {
				border: solid 1px black;
			}
			.sinborde {
				border: none !important;
			}
			.separador {
				height: 20px;
			}
			.etiqueta {
				font-weight: bold;
			}
			th {
				background-color: #dedede;
			}
		</style>
	</head>
	<body>
		<table align="center" width="100%" cellspacing="0">
			<tr>
				<td width="25%" class="sinborde">
					<img src="/logo-print.png">
				</td>
				<td width="25%" align="center">
					<h3>Nota de pedido</h3>
					<div>Expreso: <b>{{$pedido->expreso ? 'Sí':'No'}}</b></div>
					<div>Estado: <b>{{$pedido->estado}}</b></div>
				</td>
				<td width="50%">
					<table width="100%" cellspacing="0">
						<tr>
							<td align="center" class="sinborde">
								<h4>Número de pedido</h4>
								<div>{{$pedido->id}}</div>
								<h4>Cliente</h4>
								<div>{{$pedido->Cuentas->activity_ref}}</div>
							</td>
							<td align="center" class="sinborde">
								<h4>Fecha de pedido</h4>
								<div>{{$pedido->fecha->format('d/m/Y')}}</div>
								<h4>Fecha de entrega</h4>
								<div>{{Carbon\Carbon::parse($fields->entrega)->format('d/m/Y')}}</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td class="separador sinborde">&nbsp;</td></tr>
			<tr>
				<td class="sinborde" colspan="3">
					<table width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>It.</th>
								<th>Código</th>
								<th>Producto</th>
								<th>Cant.</th>
								<th>Entregado</th>
								<th>Reb.</th>
								<th>Fab.</th>
							</tr>
						</thead>
						<tbody>
							@foreach($pedido->pedidolinea as $linea)
							<tr>
								<td>{{$loop->iteration}}</td>
								<td>{{$linea->producto->codigo}}</td>
								<td>{{$linea->producto->descripcion}}</td>
								<td align="right">{{$linea->cantidad}}</td>
								<td align="right">{{$linea->entregado}}</td>
								<td align="right">{{(!empty($linea->producto->Stock)) ? $linea->producto->Stock->rebabado : 0 }}</td>
								<td align="right">{{(!empty($linea->producto->Stock)) ? $linea->producto->Stock->fabrica : 0}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</td>
			</tr>
			<tr><td class="separador sinborde">&nbsp;</td></tr>
			<tr>
				<td colspan="3">
					<table width="100%" cellspacing="0">
						<tr>
							<td width="25%" class="sinborde etiqueta">Comentarios:</td>
							<td width="75%" class="sinborde">{{$fields->comentarios}}</td>
						</tr>
						<tr>
							<td class="sinborde etiqueta">Transporte:</td>
							<td class="sinborde">{{$fields->transporte}}</td>
						</tr>
						<tr>
							<td class="sinborde etiqueta">Cantidad de Items</td>
							<td class="sinborde">{{$pedido->pedidolinea()->count()}}</td>
						</tr>
					</table>
				</td>
			</tr>
			@if(!empty($fields->caja_1) || !empty($fields->caja_2) || !empty($fields->caja_3) || !empty($fields->caja_4) || !empty($fields->caja_5) || !empty($fields->caja_6))
			<tr><td class="separador sinborde">&nbsp;</td></tr>
			<tr>
				<td colspan="3" class="sinborde">
					<table width="100%" cellspacing="0">
						<tr>
							<td width="50%">
								@if(!empty($fields->caja_1))
								<div><b>Caja 1:</b> {{$fields->caja_1}}</div> 
								@endif
								@if(!empty($fields->caja_2))
								<div><b>Caja 2:</b> {{$fields->caja_2}}</div> 
								@endif
								@if(!empty($fields->caja_3))
								<div><b>Caja 3:</b> {{$fields->caja_3}}</div> 
								@endif
							</td>
							<td width="50%">
								@if(!empty($fields->caja_4))
								<div><b>Caja 4:</b> {{$fields->caja_4}}</div> 
								@endif
								@if(!empty($fields->caja_5))
								<div><b>Caja 5:</b> {{$fields->caja_5}}</div> 
								@endif
								@if(!empty($fields->caja_6))
								<div><b>Caja 6:</b> {{$fields->caja_6}}</div> 
								@endif
							</td>
						</tr>
						
					</table>
				</td>
			</tr>
			@endif
		</table>
	</body>
</html>