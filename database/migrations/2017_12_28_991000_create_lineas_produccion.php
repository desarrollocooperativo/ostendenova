<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLineasProduccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		 Schema::create('produccion_lineas', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('produccion_id')->unsigned();
			$table->integer('producto_id')->unsigned();
            $table->decimal('cantidad', 18, 2)->default(0);
			$table->decimal('producido', 18, 2)->default(0);
            $table->integer('bocas')->default(0);
            $table->integer('moldeadas')->default(0);
            $table->foreign('producto_id')->references('id')->on('productos')->unsigned();
            $table->foreign('produccion_id')->references('id')->on('produccion')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produccion_lineas');
    }
}
