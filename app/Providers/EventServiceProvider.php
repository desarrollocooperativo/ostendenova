<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Events\StockSaving;
use App\Listeners\StockSavingListener;
use App\Events\PedidoLineaSaving;
use App\Listeners\PedidoLineaListener;
use App\Events\CompraLineaSaving;
use App\Listeners\CompraLineaListener;
use App\Events\CompraLineaSaved;
use App\Listeners\CompraLineaSavedListener;
use App\Events\CompraLineaCreated;
use App\Listeners\CompraLineaCreatedListener;
use App\Events\CompraLineaUpdated;
use App\Listeners\CompraLineaUpdatedListener;
use App\Events\ProduccionLineaSaved;
use App\Listeners\ProduccionLineaSavedListener;
use App\Events\ProduccionLineaUpdated;
use App\Listeners\ProduccionLineaUpdatedListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        StockSaving::class => [
            StockSavingListener::class,
        ],
        PedidoLineaSaving::class => [
            PedidoLineaListener::class,
        ],
        CompraLineaSaving::class => [
            CompraLineaListener::class,
        ],
        CompraLineaSaved::class => [
            CompraLineaSavedListener::class,
        ],
        CompraLineaUpdated::class => [
            CompraLineaUpdatedListener::class,
        ],
        CompraLineaCreated::class => [
            CompraLineaCreatedListener::class,
        ],
        ProduccionLineaSaved::class => [
            ProduccionLineaSavedListener::class,
        ],
        ProduccionLineaUpdated::class => [
            ProduccionLineaUpdatedListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
