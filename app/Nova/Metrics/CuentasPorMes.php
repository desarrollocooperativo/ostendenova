<?php

namespace App\Nova\Metrics;

use App\Models\Cuenta;
use Illuminate\Http\Request;
use Laravel\Nova\Metrics\Trend;

class ClientesPorMes extends Trend
{
    /**
     * Calculate the value of the metric.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function calculate(Request $request)
    {
        return $this->countByMonths($request, Cuenta::where('tipo','C'))
            ->showLatestValue();
    }

    /**
     * Get the ranges available for the metric.
     *
     * @return array
     */
    public function ranges()
    {
        return [
            3 => '3 meses',
            6 => '6 meses',
            9 => '9 mese',
            12 => '12 meses',
        ];
    }

    /**
     * Determine for how many minutes the metric should be cached.
     *
     * @return  \DateTimeInterface|\DateInterval|float|int
     */
    public function cacheFor()
    {
        // return now()->addMinutes(5);
    }

    /**
     * Get the URI key for the metric.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'cuentas-por-mes';
    }
}
