<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GenericPolicy
{
    use HandlesAuthorization;

    

    /**
     * Determine whether the user can view the cuenta.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Cuenta  $cuenta
     * @return mixed
     */
    public function view(User $user, $model)
    {
        $p = $user->permiso(get_class($model));
        return  $p == 'R' || $p == 'W';
    }

    /**
     * Determine whether the user can create cuentas.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the cuenta.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Cuenta  $cuenta
     * @return mixed
     */
    public function update(User $user, $model)
    {
        $p = $user->permiso(get_class($model));
        return  $p == 'W';
    }

    /**
     * Determine whether the user can delete the cuenta.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Cuenta  $cuenta
     * @return mixed
     */
    public function delete(User $user, $model)
    {
        $p = $user->permiso(get_class($model));
        return  $p == 'W';
    }

    /**
     * Determine whether the user can restore the cuenta.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Cuenta  $cuenta
     * @return mixed
     */
    public function restore(User $user, $model)
    {
        $p = $user->permiso(get_class($model));
        return  $p == 'W';
    }

    /**
     * Determine whether the user can permanently delete the cuenta.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Cuenta  $cuenta
     * @return mixed
     */
    public function forceDelete(User $user, $model)
    {
        $p = $user->permiso(get_class($model));
        return  $p == 'W';
    }
}
