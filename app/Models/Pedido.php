<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;


class Pedido extends Master
{
	protected $dates = ['fecha','created_at','updated_at'];
	protected $appends = ['descuento','estado'];

    public function Cuentas(){
    	return $this->belongsTo(Cuenta::class,'cuentas_id');
    }

    public function pedidolinea(){
    	return $this->hasMany(PedidoLinea::class);
    }

    public function getDescuentoAttribute(){
    	$cuenta = $this->Cuentas()->first();
    	return !empty($cuenta) ? $cuenta->descuento : 0;
    }

    public function actualizarTotales(){
    	$lineas = $this->pedidolinea()->get();
    	$subtotal = count($lineas) ? $lineas->sum('importe') : 0;
    	$descuento = $this->descuento;
    	$total = ($subtotal == 0) ? 0 : (($descuento == 0) ? $subtotal : $subtotal * ((100 - $descuento) / 100));
    	$this->subtotal = $subtotal;
    	$this->total = $total;
    	$this->save();
    }

    public function getEstadoAttribute(){
    	$lineas = $this->pedidolinea()->get();
    	if (count($lineas)) {
    		if($lineas->sum('entregado') == 0){
    			return 'pedido';
    		}
    		return $lineas->sum('cantidad') > $lineas->sum('entregado') ? 'pendiente' : 'entregado';
    	} else {
    		return 'pedido';
    	} 
    }
}
