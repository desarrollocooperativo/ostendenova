<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Status;
use App\Models\Compra;
use PDF;
use Uuid;


class ImprimirCompra extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $onlyOnDetail = true;
    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        foreach ($models as $model) {
            
            $pdf = PDF::loadView('pdf.compra', ['compra'=>$model,'fields'=>$fields]);
            $uuid = (string) Uuid::generate();
            $filename = storage_path('app/public/pdf/'.$uuid.'.pdf');
            $pdf->save($filename);
            return Action::download(url('storage/pdf/'.$uuid.'.pdf'),'compra-'.$model->id.'.pdf');

        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Select::make('Estado')->options([
                'A' => 'Abierta',
                'C' => 'Cerrada',
            ])->displayUsingLabels()->withMeta(["value" => "A"]),
            Text::make('clase')->withMeta(["value" => "Materia prima"]),
            Text::make('tipo'),
            Text::make('entrega')->withMeta(["value" => env('ENTREGA_MATERIALES')]),
            Text::make('horario')->withMeta(["value" => env('HORARIO_ENTREGA')]),
            Text::make('fecha')->withMeta(["value" => env('Inmediato')]),
        ];
    }
}
