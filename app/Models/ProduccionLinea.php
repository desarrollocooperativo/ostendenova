<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;
use App\Events\ProduccionLineaSaved;
use App\Events\ProduccionLineaUpdated;


class ProduccionLinea extends Master
{
    protected $table = "produccion_lineas";
    public function Produccion(){
        return $this->belongsTo(Produccion::class);
    }

    public function producto(){
    	return $this->belongsTo(Producto::class);
    }

    public function producir(){
    	$this->producido = $this->cantidad;
    	$this->save();
    }

    protected $dispatchesEvents = [
        'saved' => ProduccionLineaSaved::class,
        'updated' => ProduccionLineaUpdated::class,
        ];

}
