<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class Migrador extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrador';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migra datos desde sistema anterior';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::statement("SET foreign_key_checks=0");
        DB::table('cuentas')->truncate();
        DB::table('contactos')->truncate();
        DB::table('categorias')->truncate();
        DB::table('productos')->truncate();
        DB::table('stock')->truncate();
        DB::table('stock_movimientos')->truncate();
        DB::table('pedidos')->truncate();
        DB::table('pedido_lineas')->truncate();
        DB::table('compras')->truncate();
        DB::table('compra_lineas')->truncate();
        DB::table('produccion')->truncate();
        DB::table('produccion_lineas')->truncate();
        DB::statement("SET foreign_key_checks=1");
        $this->info('tablas truncadas');
        $this->cuentas();
        $this->contactos();
        $this->categorias();
        $this->productos();
        $this->stock();
        $this->pedidos();
        $this->compras();
        $this->produccion();
    }

    public function categorias(){
        $query = "
            insert into ostendenova.categorias(
                categoria
            )
            select distinct CONVERT(CAST(CONVERT(categoria USING latin1) AS BINARY) USING utf8) from ostende.productos where deleted = 0 and categoria != '' order by CONVERT(CAST(CONVERT(categoria USING latin1) AS BINARY) USING utf8);
        ";
        DB::insert($query);
        $this->info('tabla categorias importada');

    }

    public function productos(){
        $query = "
                Insert into ostendenova.productos (
                  id,
                  tipo,
                  descripcion,
                  codigo,
                  cod_mat,
                  medidas,
                  origen,
                  bocas,
                  stock_min,
                  stock_max,
                  codigo_proveedor,
                  moneda,
                  precio,
                  precio_proveedor,
                  unidad_id,
                  cuenta_id,
                  categoria_id,
                  created_at,
                  updated_at
                )
                select 
                  id,
                  tipo,
                  descripcion,
                  codigo,
                  cod_mat,
                  medidas,
                  origen,
                  bocas,
                  min_dias,
                  max_dias,
                  codigo_prov,
                  moneda,
                  precio,
                  precio_prov,
                  unidad + 1,
                  case when prov_id = 0 then null else prov_id end,
                  (select c.id from ostendenova.categorias c where c.categoria = CONVERT(CAST(CONVERT(p.categoria USING latin1) AS BINARY) USING utf8) COLLATE utf8_unicode_ci ),
                  now(),now()
                  from ostende.productos as p where deleted = 0 and prov_id not in (select id from ostende.agenda where deleted = 1)";
                    
            DB::insert($query);

        $query = "
            insert into ostendenova.formulas (cantidad,producto_id,componente_id)
            select cantidad,producto_id,componente_id from ostende.componentes where producto_id not in (select id from ostende.productos where deleted = 1) and componente_id not in (select id from ostende.productos where deleted = 1)
        ";
        DB::insert($query);

        $this->info('tabla productos importada');
    }

    public function cuentas(){
        $query = "
            Insert into ostendenova.cuentas (
                id,
                tipo,
                razon_social,
                empresa,
                codigo,
                cuit,
                domicilio,
                localidad,
                codpos,
                x_provincia,
                telefono,
                fax,
                celular,
                email,
                email2,
                web,
                descuento,
                condicion_pago,
                observaciones,
                created_at,
                updated_at
            ) 
            select 
                id,
                case when tipo = 0 then 'C' else 'P' end as tipo,
                case when razon_social is null || trim(razon_social) = '' then case when clave_fiscal is null || clave_fiscal = '' then concat(nombre,' ',apellido) else clave_fiscal end else razon_social end  as razon_social,
                ifnull(clave_fiscal,concat(nombre,' ',apellido)) as empresa,
                codigo,
                cuit,
                concat(direccion,' ',dir_numero) as domicilio,
                localidad,
                zip as codpos,
                provincia,
                tel as telefono,
                cel_uno as fax,
                cel_dos as celular,
                mail_uno as email,
                mail_dos email2,
                web,
                case when descuento = ''  then 0 else descuento end as descuento,
                condiciones,
                observaciones,
                ifnull(fecha_ingreso,now()) as created_at,
                now()
            from ostende.agenda 
            where deleted = 0";
        DB::insert($query);

        $query = "update ostendenova.cuentas c set c.provincias_id = (select max(p.id) from provincias p where p.x_provincia like concat('%',c.x_provincia,'%')) where c.x_provincia != '' and x_provincia is not null";
        DB::update($query);

        $this->info('tabla cuentas importada');
    }

    public function contactos(){
        $query = "
            Insert into ostendenova.contactos (
                cuentas_id,
                nombre,
                apellido,
                telefono,
                celular,
                email,
                created_at,
                updated_at
            ) 
            select 
                id,
                contacto,
                '',
                tel as telefono,
                cel_dos as celular,
                mail_uno as email,
                ifnull(fecha_ingreso,now()) as created_at,
                now()
            from ostende.agenda 
            where deleted = 0 and contacto is not null and contacto != ''";
        DB::insert($query);

        $this->info('tabla contactos importada');
    }

    public function stock(){
        $query = "
            Insert into ostendenova.stock (
                producto_id,
                rebabado,
                fabrica,
                created_at,
                updated_at
            ) 
            select 
                s.prod_id,
                s.reb,
                s.fab,
                now(),
                now()
            from ostende.stock s
            where s.deleted = 0 and s.prod_id in (select id from ostendenova.productos)";
        DB::insert($query);

        $this->info('tabla stock importada');
    }

    public function pedidos(){
        $query = "Insert into ostendenova.pedidos (id,cuentas_id,fecha,expreso,estado,observaciones,subtotal,total,created_at,updated_at)
          select id, cliente_id,fecha,case when expreso = 'No' then 0 else 1 end, estado,observaciones,subtotal,monto,now(),now()
          from ostende.pedidos where deleted = 0 and cliente_id in (select id from ostende.agenda where deleted = 0 )
        ";
        DB::insert($query);

        $query = "Insert into ostendenova.pedido_lineas (pedido_id,producto_id,cantidad,entregado,precio,importe,iva,created_at,updated_at)
          select pedido_id,prod_id,cantidad,entregado,precio,cantidad * precio,replace(iva,'%',''),now(),now() 
          from ostende.lista_pedido where pedido_id in (select id from ostende.pedidos where deleted = 0 and cliente_id in (select id from ostende.agenda where deleted = 0 )) and prod_id in (select id from ostendenova.productos)
        ";

        DB::insert($query);

        $this->info('tabla pedidos importada');
    }

    public function compras(){
        $query = "Insert into ostendenova.compras (id,cuentas_id,fecha,destino,observaciones,subtotal,total,created_at,updated_at)
          select id, proveedor_id,fecha,case when destination = '' then null else left(destination,3) end, observaciones,subtotal,monto,now(),now()
          from ostende.compras where deleted = 0 and proveedor_id in (select id from ostende.agenda where deleted = 0 and tipo > 0 )
        ";

        DB::insert($query);

        $query = "Insert into ostendenova.compra_lineas (compra_id,producto_id,cantidad,entregado,precio,subtotal,total_iva,total,iva,created_at,updated_at)
          select compra_id,producto_id,cantidad,entregado,precio,cantidad * precio as subtotal,
(cantidad * precio * convert(replace(iva,'%',''),decimal(18,2)) / 100) as total_iva,
cantidad * precio * (1 + (convert(replace(iva,'%',''),decimal(18,2)) /100)) as total,
replace(iva,'%','') as iva,now(),now() 
          from ostende.lista_de_compra where compra_id in (select id from ostende.compras where deleted = 0 and proveedor_id in (select id from ostende.agenda where deleted = 0 and tipo > 0)) and producto_id in (select id from ostendenova.productos)
        ";

        DB::insert($query);

        $this->info('tabla compras importada');
    }

    public function produccion(){
        $query = "Insert into ostendenova.produccion (id,fecha,estado,observaciones,created_at,updated_at)
          select id,fecha,left(estado,1), observaciones,fecha,fecha
          from ostende.fabricacion where deleted = 0  
        ";

        DB::insert($query);

        $query = "Insert into ostendenova.produccion_lineas (produccion_id,producto_id,cantidad,producido,bocas,moldeadas,created_at,updated_at)
          select orden_id,prod_id,cantidad,entregado,bocas,mold,now(),now() 
          from ostende.lista_fab where orden_id in (select id from ostende.fabricacion where deleted = 0  and prod_id in (select id from ostendenova.productos))
        ";

        DB::insert($query);

        $query = "update ostendenova.produccion set producido = (select ifnull(sum(producido),0) from ostendenova.produccion_lineas where ostendenova.produccion_lineas.produccion_id = ostendenova.produccion.id),cantidad = (select ifnull(sum(cantidad),0) from ostendenova.produccion_lineas where ostendenova.produccion_lineas.produccion_id = ostendenova.produccion.id);
        ";

        DB::update($query);

        $this->info('tabla produccion importada');
    }
}
