<?php

namespace App\Listeners;

use App\Events\CompraLineaCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\CompraLineaCreated as CompraLineaCreatedEvent;
use App\Models\CompraLinea;
use App\Models\Stock;

class CompraLineaCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CompraLineaCreated  $event
     * @return void
     */
    public function handle(CompraLineaCreatedEvent $event)
    {
        if($event->compra_linea->entregado > 0){
            if(empty($event->compra_linea->Producto->Stock)){
                Stock::create(['producto_id'=>$event->compra_linea->Producto->id,'rebabado'=> 0,'fabrica'=>$event->compra_linea->entregado]);
            } else {
                $s = Stock::where('producto_id','=',$event->compra_linea->Producto->id)->first();
                $s->fabrica += $event->compra_linea->entregado;
                $s->save();
            }
        }
    }
}
