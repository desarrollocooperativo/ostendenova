<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provincias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('provincia');
            $table->string('x_provincia');
            $table->timestamps();
        });
        Schema::create('cuentas', function (Blueprint $table) {
           	$table->increments('id');
            $table->char('tipo',1);
            $table->string('razon_social')->nullable();
			$table->string('empresa')->nullable();
            $table->string('cuit')->nullable();
			$table->string('codigo')->nullable();
            $table->string('domicilio')->nullable();
			$table->string('localidad')->nullable();
			$table->string('codpos')->nullable();
            $table->string('x_provincia')->nullable();
            $table->integer('provincias_id')->unsigned()->nullable();
			$table->string('telefono')->nullable();
			$table->string('fax')->nullable();
            $table->string('celular')->nullable();
            $table->string('email')->nullable();
            $table->string('email2')->nullable();
            $table->string('web')->nullable();
            $table->decimal('descuento',14,2)->default(0);
            $table->string('condicion_pago')->nullable();
            $table->text('observaciones')->nullable();
            $table->foreign('provincias_id')->references('id')->on('provincias')->unsigned();
			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuentas');
        Schema::dropIfExists('provincias');
    }
}
