<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Code;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\DateTime;

class Activity extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'Spatie\Activitylog\Models\Activity';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'description';
    public static $group = 'LOG';


    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'description','subject_type','properties',
    ];

    public static $globallySearchable = false;

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $fields =  [
            ID::make()->sortable(),
            Text::make('Acción',function(){
                $acciones = [
                    'created' => 'Creado',
                    'updated' => 'Actualizado',
                    'deleted' => 'Eliminado',
                ];
                return isset($acciones[$this->description]) ? $acciones[$this->description] : $this->description ;
            })->sortable(),
            Text::make('Tipo',function(){
                return str_replace('App\\Models\\', '', $this->subject_type);
            })->sortable(),
            Text::make('Modelo',function(){
                return (empty($this->subject)) ? null : $this->subject->id;
            })->sortable(),
            Text::make('Usuario',function(){
                return $this->causer->name;
            })->sortable(),
            DateTime::make('Fecha/Hora','created_at')->sortable(),
            Code::make('Propiedades',function(){
                return json_encode($this->properties['attributes'], JSON_PRETTY_PRINT);
            })->hideFromIndex(),
            

        ];

        if(isset($this->properties['old'])){
            $fields[] = Code::make('Anterior',function(){
                return json_encode($this->properties['old'], JSON_PRETTY_PRINT);
            })->hideFromIndex();
        }

        return $fields;
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new Filters\ActivityUser,
            new Filters\ActivityTipo,
            new Filters\ActivityDesde,
            new Filters\ActivityHasta,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
