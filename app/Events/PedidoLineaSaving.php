<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Models\PedidoLinea;

class PedidoLineaSaving
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $pedido_linea;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(PedidoLinea $pedido_linea)
    {
        $this->pedido_linea = $pedido_linea;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
