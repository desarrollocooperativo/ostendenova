Nova.booting((Vue, router, store) => {
    Vue.component('index-bocas-moldeadas-cantidad', require('./components/IndexField'))
    Vue.component('detail-bocas-moldeadas-cantidad', require('./components/DetailField'))
    Vue.component('form-bocas-moldeadas-cantidad', require('./components/FormField'))
})
