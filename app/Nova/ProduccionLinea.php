<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;


use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Status;

use Cardumen\BocasMoldeadasCantidad\BocasMoldeadasCantidad;
use Titasgailius\SearchRelations\SearchesRelations;


class ProduccionLinea extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\ProduccionLinea';

    public static $globallySearchable = false;
    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';
    public static $displayInNavigation = false;
    public static $group = '';


    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        //esto tiene que venir luego de bocas y cantidad y moldeadas para acceder a sus valores
        $fields = [
            ID::make()->sortable(),
            BelongsTo::make('Produccion')->rules('required')->searchable(),
            BelongsTo::make('Producto')->searchable()->exceptOnForms(),
            Number::make('bocas')->exceptOnForms(),
            Number::make('moldeadas')->exceptOnForms(),
            Number::make('cantidad')->rules('required')->exceptOnForms(),
        ];
        $fields[] = BocasMoldeadasCantidad::make('bocas-moldeadas-cantidad')->producto((isset($this->producto)) ? $this->producto->id : null)->bocas($this->bocas,(isset($this->producto)) ? $this->producto->id : null)->moldeadas($this->moldeadas)->cantidad($this->cantidad)->hideFromDetail()->hideFromIndex();
        $fields[] = Number::make('producido')->rules('required')->hideWhenCreating();
        return $fields;
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new Actions\ProducirProduccionLinea,
        ];
    }
}
