<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;


class Compra extends Master
{
	protected $dates = ['fecha','created_at','updated_at'];
	protected $appends = ['estado'];

    public function Cuentas(){
    	return $this->belongsTo(Cuenta::class,'cuentas_id');
    }

    public function compralinea(){
    	return $this->hasMany(CompraLinea::class);
    }

    
    public function actualizarTotales(){
    	$lineas = $this->compraLinea()->get();
    	$subtotal = count($lineas) ? $lineas->sum('subtotal') : 0;
    	$total = count($lineas) ? $lineas->sum('total') : 0;
    	$this->subtotal = $subtotal;
    	$this->total = $total;
    	$this->save();
    }

    public function getEstadoAttribute(){
    	$lineas = $this->compraLinea()->get();
    	if (count($lineas)) {
    		if($lineas->sum('entregado') == 0){
    			return 'pedido';
    		}
    		return $lineas->sum('cantidad') > $lineas->sum('entregado') ? 'incompleto' : 'recibido';
    	} else {
    		return 'pedido';
    	} 
    }
}
