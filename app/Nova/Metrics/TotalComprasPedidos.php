<?php

namespace App\Nova\Metrics;

use Illuminate\Http\Request;
use Laravel\Nova\Metrics\Value;
use App\Models\Compra;
use App\Models\CompraLinea;
use DB;

class TotalComprasPedidos extends Value
{
    /**
     * Calculate the value of the metric.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function calculate(Request $request)
    {
        return $this->sum($request, Compra::whereIn('id', function($query){
                $query->select('id')
                ->from('compras')
                ->whereIn('id', array_map(function ($c){
                    return $c->compra_id;
                },
                    DB::select("SELECT compra_lineas.compra_id FROM compra_lineas  group by compra_lineas.compra_id having sum(compra_lineas.entregado) = 0")));
            }), 'total','fecha')->prefix('$')->format('0.00');
    }

    /**
     * Get the ranges available for the metric.
     *
     * @return array
     */
    public function ranges()
    {
        return [
            1 => '24 hs',
            7 => 'Ult. 7 días',
            30 => '30 Días',
            60 => '60 Días',
            365 => '365 Días',
        ];
    }

    /**
     * Determine for how many minutes the metric should be cached.
     *
     * @return  \DateTimeInterface|\DateInterval|float|int
     */
    public function cacheFor()
    {
        // return now()->addMinutes(5);
    }

    /**
     * Get the URI key for the metric.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'total-compras-pedidos';
    }
}
