<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\DateFilter;


class PedidosDesde extends DateFilter
{


    public function apply(Request $request, $query, $value)
    {
        return $query->where('fecha','>=', $value);
    }
    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
  	}
}
